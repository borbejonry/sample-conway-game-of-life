using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ViewManager : MonoBehaviour
{
    public event Action OnBegin;
    public event Action OnReset;

    public event Action<float> OnSpeedChanged;

    [SerializeField] GameObject cellToClone;
    [SerializeField] Transform groupContent;

    [SerializeField] GridLayoutGroup group;

    [SerializeField] Button startAndRestart;

    [SerializeField] TMP_InputField gridX;
    [SerializeField] TMP_InputField gridY;

    [SerializeField] TMP_InputField cellSize;

    [SerializeField] TMP_InputField speed;

    [SerializeField] TextMeshProUGUI cellStatus;
    bool isStart = false;

    Vector2 grid = new Vector2(0,0);

    private void Awake()
    {
        startAndRestart.onClick.AddListener(OnPressStart);

        gridX.onValueChanged.AddListener(GridXValueChanged);
        gridY.onValueChanged.AddListener(GridYValueChanged);

        cellSize.onValueChanged.AddListener(CellSizeValueChanged);

        speed.onValueChanged.AddListener(SpeedChanged);

    }

    private void Start()
    {

        ColumnUpdate(int.Parse(gridX.text));
        CellSizeUpdate(int.Parse(cellSize.text));

        grid.x = int.Parse(gridX.text);
        grid.y = int.Parse(gridY.text);
    }

    private void SpeedChanged(string arg0)
    {
        if (arg0 == "")
        {
            arg0 = "0";
        }

        OnSpeedChanged.Invoke(float.Parse(arg0));
    }

    private void CellSizeValueChanged(string arg0)
    {
        if (arg0 == "")
        {
            arg0 = "0";
        }

        CellSizeUpdate(int.Parse(arg0));
    }

    private void GridXValueChanged(string arg0)
    {
        if (arg0 == "")
        {
            arg0 = "0";
        }

        int temp = int.Parse(arg0);
        grid.x = temp;
        ColumnUpdate(temp);

    }
    private void GridYValueChanged(string arg0)
    {
        if (arg0 == "")
        {
            arg0 = "0";
        }

        int temp = int.Parse(arg0);
        grid.y = temp;
    }

    private void OnPressStart()
    {
        isStart = !isStart;

        if (isStart)
        {
            OnBegin.Invoke();
            startAndRestart.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Reset";

            gridX.interactable = false;
            gridY.interactable = false;
            cellSize.interactable = false;
        }
        else
        {
            OnReset.Invoke();
            startAndRestart.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Begin";

            gridX.interactable = true;
            gridY.interactable = true;
            cellSize.interactable = true;
        }
    }

    public Vector2 GetGrid() 
    {
        return grid;
    }

    private void ColumnUpdate(int val)
    {
        group.constraintCount = val;
    }
    private void CellSizeUpdate(int val)
    {
        group.cellSize = new UnityEngine.Vector2(val, val);
    }

    public void UpdateCellStatus(int val1,int val2)
    {
        cellStatus.text = val1 + "/" + val2;
    }

    public void PlaceCells(Cell _cell)
    {

        Debug.Log("Instantiate");
        GameObject _obj = Instantiate(cellToClone, groupContent);
        //_obj.name = "Cell-" + x + "-" + y;
        _obj.SetActive(true);

        _cell.obj = _obj;
        _cell.UpdateCell();
    }

    public void ResetCells(Cell[,] grid, int gridW, int gridH)
    {
        for (int y = 0; y < gridH; y++)
        {
            for (int x = 0; x < gridW; x++)
            {
                Destroy(grid[x, y].obj);
            }
        }
    }
}
