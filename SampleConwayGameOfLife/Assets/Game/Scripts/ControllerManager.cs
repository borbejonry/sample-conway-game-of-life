using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    [SerializeField] ViewManager view;

    GameModel model;

    Vector2 status;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        model = new GameModel();

        view.OnBegin += OnBegin;
        view.OnReset += OnReset;

        view.OnSpeedChanged += OnSpeedChanged;

        model.OnCellsCreated += CellsCreated;
    }

    private void CellsCreated(Cell[,] obj)
    {
        Debug.Log("Create cells");

        for (int y = 0; y < model.gridH; y++)
        {
            for (int x = 0; x < model.gridW; x++)
            {
                view.PlaceCells(obj[x,y]);
            }
        }
    }
    private void Update()
    {
        if (model.isGameBegun)
        {
            if (model.speed != 0)
            {
                if (timer >= model.speed)
                {
                    timer = 0;

                    model.CountNeighbors();

                    model.PopulationControl();

                    model.CellStatus();

                    UpdateCellsVisual(model.GetCells());
                }
                else
                {
                    timer += Time.deltaTime;
                }
            }
        }

        if (model.isGameBegun)
        {
            status = model.CellStatus();
            view.UpdateCellStatus((int)status.x, (int)status.y);
        }
        else
        {
            view.UpdateCellStatus(0,0);
        }
    }

    void UpdateCellsVisual(Cell[,] grid)
    {
        for (int y = 0; y < model.gridH; y++)
        {
            for (int x = 0; x < model.gridW; x++)
            {
                grid[x, y].UpdateCell();
            }
        }
    }

    private void OnSpeedChanged(float val)
    {
        model.speed = val;
    }

    private void OnBegin()
    {
        model.BeginGame(view.GetGrid());
        timer = 0;
        model.isGameBegun = true;
    }
    private void OnReset()
    {
        model.isGameBegun = false;

        view.ResetCells(model.GetCells(),model.gridW,model.gridH);
    }
}
