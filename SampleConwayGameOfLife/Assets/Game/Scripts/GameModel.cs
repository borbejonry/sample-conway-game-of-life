public class GameModel
{
    public event System.Action<Cell[,]> OnCellsCreated;

    public int gridW = 25;
    public int gridH = 25;

    Cell[,] grid;

    public float speed = 1f;

    public bool isGameBegun = false;


    public Cell[,] GetCells()
    {
        return grid;
    }

    public Vector2 CellStatus()
    {
        Vector2 temp = new Vector2(0, 0);
        for (int y = 0; y < gridH; y++)
        {
            for (int x = 0; x < gridW; x++)
            {
                if (grid[x, y].isAlive)
                {
                    temp.x += 1;
                }

                temp.y += 1;
            }
        }

        return temp;
    }

    public void BeginGame(Vector2 val)
    {
        gridW = (int)val.x;
        gridH = (int)val.y;

        CreateCells();
        CountNeighbors();
        
        OnCellsCreated.Invoke(grid);
    }

    public void CreateCells()
    {
        grid = new Cell[gridW, gridH];
        System.GC.Collect();

        for (int y = 0; y < gridH; y++)
        {
            for (int x = 0; x < gridW; x++)
            {
                Cell _cell = new Cell();
                grid[x, y] = _cell;

                grid[x, y].SetAlive(RandomAliveCell());
            }
        }
    }

    bool RandomAliveCell()
    {
        int rand = UnityEngine.Random.Range(0, 100);

        if (rand > 75 )
        {
            return true;
        }

        return false;
    }

    public void CountNeighbors()
    {
        for (int y = 0; y < gridH; y++)
        {
            for (int x = 0; x < gridW; x++)
            {
                int numNeighbors = 0;

                //north
                if (y + 1 < gridH)
                {
                    if (grid[x, y + 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //south
                if (y - 1 >= 0)
                {
                    if (grid[x, y - 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //east
                if (x + 1 < gridW)
                {
                    if (grid[x + 1, y].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //west
                if (x - 1 >= 0)
                {
                    if (grid[x - 1, y].isAlive)
                    {
                        numNeighbors++;
                    }
                }


                //north east
                if (x + 1 < gridW && y + 1 < gridH)
                {
                    if (grid[x + 1, y + 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //north west
                if (x - 1 >= 0 && y + 1 < gridH)
                {
                    if (grid[x - 1, y + 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //south east
                if (x + 1 < gridW && y - 1 >= 0)
                {
                    if (grid[x + 1, y - 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                //south west
                if (x - 1 >= 0 && y - 1 >= 0)
                {
                    if (grid[x - 1, y - 1].isAlive)
                    {
                        numNeighbors++;
                    }
                }

                grid[x, y].numNeighbors = numNeighbors;
            }
        }
    }

    public void PopulationControl()
    {
        for (int y = 0; y < gridH; y++)
        {
            for (int x = 0; x < gridW; x++)
            {
                //rules

                if (grid[x, y].isAlive)
                {
                    //Any live cell with fewer than two live neighbours dies, as if by underpopulation
                    if ((grid[x, y].numNeighbors <= 1))
                    {
                        grid[x, y].SetAlive(false);
                    }

                    ////Any live cell with two or three live neighbours lives on to the next generation.
                    //if ((grid[x, y].numNeighbors != 3)&&(grid[x, y].numNeighbors != 2))
                    //{
                    //    grid[x, y].SetAlive(false);
                    //}

                    //Any live cell with more than three live neighbours dies, as if by overpopulation.
                    if ((grid[x, y].numNeighbors > 3))
                    {
                        grid[x, y].SetAlive(false);
                    }
                }
                else
                {
                    //Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                    if (grid[x, y].numNeighbors == 3)
                    {
                        grid[x, y].SetAlive(true);
                    }
                }
            }
        }
    }
}

public class Vector2
{
    public float x;
    public float y;

    public Vector2(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
}