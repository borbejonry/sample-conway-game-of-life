using UnityEngine;
using UnityEngine.UI;

public class Cell
{
    public bool isAlive = false;
    public int numNeighbors = 0;

    public void SetAlive(bool alive)
    {
        isAlive = alive;
    }

    public GameObject obj;
    public void UpdateCell()
    {
        if (obj != null)
        {
            if (isAlive)
            {
                obj.GetComponent<Image>().enabled = true;
            }
            else
            {
                obj.GetComponent<Image>().enabled = false;
            }
        }
    }
}
